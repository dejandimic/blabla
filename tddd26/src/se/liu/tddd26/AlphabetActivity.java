package se.liu.tddd26;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class AlphabetActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alphabet);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.alphabet, menu);
		return true;
	}
	
	public void playSound(View view) {
		MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.g); 
		mp.start();
	}

}
