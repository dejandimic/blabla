package se.liu.tddd26;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class ChoicesAlphabetActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choices_alphabet);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.choices_alphabet, menu);
		return true;
	}
	
	public void towardTutorial(View view) {
		Intent intent = new Intent(this, AlphabetActivity.class);
    	startActivity(intent);
	}
	
	public void towardTest(View view) {
		Intent intent = new Intent(this, TestAlphabetActivity.class);
    	startActivity(intent);
	}

}
