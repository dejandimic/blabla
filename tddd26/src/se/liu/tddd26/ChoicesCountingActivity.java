package se.liu.tddd26;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class ChoicesCountingActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choices_counting);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.choices_counting, menu);
		return true;
	}
	
	public void towardTutorial(View view) {
		Intent intent = new Intent(this, CountingActivity.class);
    	startActivity(intent);
	}
	
	public void towardTest(View view) {
		Intent intent = new Intent(this, TestCountingActivity.class);
    	startActivity(intent);
	}

}
